The MGI Query Builder is a toolkit to make generating, executing and parsing database queries easy. It uses a variety of variant parsing techniques to automatically handle all of the annoying parts of getting database data converted into LabVIEW data types.

## Contributing

See [Contributing.md](CONTRIBUTING.md) for information on how to submit pull requests. Bugs can be reported using the repositories issue tracker.

#### _This package is implemented with LabVIEW 2017_
